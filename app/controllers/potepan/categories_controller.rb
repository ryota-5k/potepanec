class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxonomy.includes(:taxons)
    @taxon = Spree::Taxon.includes(:products).find(params[:id])
  end
end
