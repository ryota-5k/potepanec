class Potepan::ProductsController < ApplicationController
  MAXIMUM_PRODUCT_COUNT = 4
  def show
    @product = Spree::Product.find(params[:id])
    @variant = @product.master
    @related_products = @product.related_products.limit(MAXIMUM_PRODUCT_COUNT)
  end
end
