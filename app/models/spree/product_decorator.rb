Spree::Product.module_eval do
  def related_products
    Spree::Product.includes(master: [:default_price, :images]).joins(:taxons).where.not(id: id).
      where(spree_products_taxons: { taxon_id: taxons.ids }).distinct
  end
end
