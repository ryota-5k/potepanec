require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show' do
    let(:taxon) { create(:taxon) }
    let(:taxonomies) { Spree::Taxonomy.all }

    before do
      get :show, params: { id: taxon.id }
    end

    it '@taxonが正しく定義されていること' do
      expect(controller.instance_variable_get("@taxon")).to eq taxon
    end

    it '@taxonomiesが正しく定義されていること' do
      expect(controller.instance_variable_get("@taxonomies")).to eq taxonomies
    end

    it_behaves_like 'レスポンスのステータスが「200(成功)」であること'

    it_behaves_like 'showアクションを行うと正しいページが表示されること'
  end
end
