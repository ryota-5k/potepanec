require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it '@productが正しく定義されていること' do
      expect(controller.instance_variable_get("@product")).to eq product
    end

    it_behaves_like 'レスポンスのステータスが「200(成功)」であること'

    it_behaves_like 'showアクションを行うと正しいページが表示されること'
  end
end
