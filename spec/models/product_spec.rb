require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:main_product) { create(:product, taxons: taxons) }
  let(:taxons) { create_list(:taxon, 2) }
  let(:related_products) { create_list(:product, 5) }

  before do
    related_products_into_taxon
  end

  describe 'def related_products' do
    context 'メイン商品に対しカテゴリーが２つあり、それぞれに同じ商品群が5個ずつ入っている時' do
      it '返された配列の要素が5個の関連商品と同じこと' do
        expect([main_product.related_products[0..4]]).to all(eq related_products[0..4])
      end

      it '返された配列に重複がないこと' do
        expect(main_product.related_products.uniq).to eq main_product.related_products
      end
    end
  end
end
