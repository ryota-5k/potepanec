def related_products_into_taxon
  taxons.each do |taxon|
    taxon.products << related_products
  end
end
