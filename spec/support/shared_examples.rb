# system
shared_examples 'ヘッダーにホームへのリンクが２つあること' do
  it { expect(page).to have_link("logo", href: potepan_path) }
  it do
    within '.navbar-right' do
      expect(page).to have_link("HOME", href: potepan_path)
    end
  end
end

shared_examples 'ヘッダー下にHOMEというリンクがあること' do
  it do
    within '.col-xs-6 .breadcrumb' do
      expect(page).to have_link("HOME", href: potepan_path)
    end
  end
end

shared_examples 'ヘッダー下のHOMEをクリックするとホームへ移動できること' do
  it do
    within '.col-xs-6 .breadcrumb' do
      click_link "HOME"
    end
    expect(current_path).to eq potepan_path
  end
end

# controller
shared_examples 'レスポンスのステータスが「200(成功)」であること' do
  it { expect(response).to have_http_status "200" }
end

shared_examples 'showアクションを行うと正しいページが表示されること' do
  it { expect(response).to render_template("show") }
end
