require 'rails_helper'

RSpec.describe 'カテゴリーページ', type: :system do
  let(:taxonomies) { Spree::Taxonomy.all }
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product) }
  let(:variant) { product.master }

  before do
    taxon.products = [product]
    # 画像表示のテストを行うためにimageにalt属性を渡す
    variant.images = [create(:image, alt: "spec_image")]
    visit potepan_category_path(taxon.id)
  end

  describe 'ホームへのリンク' do
    it_behaves_like 'ヘッダーにホームへのリンクが２つあること'

    it_behaves_like 'ヘッダー下にHOMEというリンクがあること'

    it_behaves_like 'ヘッダー下のHOMEをクリックするとホームへ移動できること'
  end

  describe '商品カテゴリーの表示' do
    describe 'taxonomyの表示' do
      it '商品カテゴリーの項目に表示されていること' do
        expect(page).to have_selector('.side-nav li a', text: taxonomies.first.name)
      end
    end

    describe 'taxonの表示' do
      it 'ヘッダー下(右端)で表示されていること' do
        expect(page).to have_selector('.col-xs-6 .breadcrumb', text: taxon.name)
      end

      it 'ヘッダー下のh2タグで表示されていること' do
        expect(page).to have_selector('h2', text: taxon.name)
      end

      it '商品カテゴリーの項目をクリックすると、taxonが表示されること' do
        expect(page).to have_selector('.side-nav li a', visible: false, text: taxon.name)
        click_on taxonomies.first.name
        expect(page).to have_selector('.side-nav li a', visible: true, text: taxon.name)
      end
    end
  end

  describe '商品情報' do
    describe '商品情報の表示' do
      it 'h5タグで商品名が表示されていること' do
        expect(page).to have_selector('h5', text: product.name)
      end

      it 'h3タグで金額が表示されていること' do
        expect(page).to have_selector('h3', text: product.price)
      end

      it '画像が表示されていること' do
        expect(page).to have_selector("img[alt='spec_image']")
      end
    end

    describe '商品詳細画面へのリンク' do
      it '商品詳細画面に移動できること' do
        find("#spec_product_link").click
        expect(current_path).to eq potepan_product_path(product.id)
      end
    end
  end
end
