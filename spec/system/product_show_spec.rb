require 'rails_helper'

RSpec.describe '商品詳細画面', type: :system do
  let(:product) { create(:product, taxons: taxons) }
  let(:variant) { product.master }
  let(:taxons) { create_list(:taxon, 2) }

  before do
    # 画像表示のテストを行うためにimageにalt属性を渡す
    variant.images = [create(:image, alt: "spec_image")]
    visit potepan_product_path(product.id)
  end

  describe 'ホームへのリンク' do
    it_behaves_like 'ヘッダーにホームへのリンクが２つあること'

    it_behaves_like 'ヘッダー下にHOMEというリンクがあること'

    it_behaves_like 'ヘッダー下のHOMEをクリックするとホームへ移動できること'
  end

  describe 'カテゴリーページへのリンク' do
    it '一覧ページへ戻るをクリックすると、商品が属するカテゴリーの一覧ページに移動できること' do
      within '.media-body .list-inline' do
        click_link '一覧ページへ戻る'
      end
      expect(current_path).to eq potepan_category_path(taxons.first.id)
    end
  end

  describe '商品情報の表示' do
    context '商品名の表示' do
      it '2つのh2タグで表示されていること' do
        expect(page).to have_selector('h2', text: product.name, count: 2)
      end

      it '"ヘッダー下（右端）で表示されていること' do
        expect(page).to have_selector('.col-xs-6 .breadcrumb', text: product.name)
      end
    end

    context '金額の表示' do
      it '商品名の下のh3タグで表示されていること' do
        expect(page).to have_selector('h3', text: product.price)
      end
    end

    context '画像の表示' do
      it '１つの画像が２箇所で表示されていること' do
        expect(page).to have_selector("img[alt='spec_image']", count: 2)
      end
    end
  end
end
