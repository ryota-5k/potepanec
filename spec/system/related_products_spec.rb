require 'rails_helper'

RSpec.describe '商品詳細画面の関連商品', type: :system do
  let(:main_product) { create(:product, taxons: taxons) }
  let(:taxons) { create_list(:taxon, 2) }
  # 関連商品が４種類まで表示されることをテストするため、商品を５個作成
  let(:related_products) { create_list(:product, 5) }
  let(:variants) do
    related_products.each do |related_product|
      related_product.master
    end
  end

  before do
    related_products_into_taxon
    variants.each_with_index do |variant, i|
      variant.images << [create(:image, alt: "spec_image_#{i}")]
    end
    visit potepan_product_path(main_product.id)
  end

  describe '関連商品項目での表示' do
    it '商品名が４種類表示されていること' do
      expect(page).to have_selector(".productCaption h5", count: 4)
      expect(page).to have_selector(".productCaption h5", text: related_products[0].name)
      expect(page).to have_selector(".productCaption h5", text: related_products[1].name)
      expect(page).to have_selector(".productCaption h5", text: related_products[2].name)
      expect(page).to have_selector(".productCaption h5", text: related_products[3].name)
    end

    it '値段が４種類表示されていること' do
      # 5種類全て同じ値段が入っているので、count: 4 で１度だけ確認する
      expect(page).to have_selector(".productCaption h3", text: related_products[0].price, count: 4)
    end

    it '画像が４種類表示されていること' do
      expect(page).to have_selector("img[alt='spec_image_0']")
      expect(page).to have_selector("img[alt='spec_image_1']")
      expect(page).to have_selector("img[alt='spec_image_2']")
      expect(page).to have_selector("img[alt='spec_image_3']")
      expect(page).not_to have_selector("img[alt='spec_image_4']")
    end
  end

  describe '関連商品詳細画面へのリンク' do
    it '左端の商品情報をクリックすると、詳細画面に移動できること' do
      (all('.col-sm-6 a')[0]).click
      expect(current_path).to eq potepan_product_path(related_products[0].id)
    end

    it '左端から2番目の商品情報をクリックすると、詳細画面に移動できること' do
      (all('.col-sm-6 a')[1]).click
      expect(current_path).to eq potepan_product_path(related_products[1].id)
    end

    it '左端から3番目の商品情報をクリックすると、詳細画面に移動できること' do
      (all('.col-sm-6 a')[2]).click
      expect(current_path).to eq potepan_product_path(related_products[2].id)
    end

    it '左端から4番目の商品情報をクリックすると、詳細画面に移動できること' do
      (all('.col-sm-6 a')[3]).click
      expect(current_path).to eq potepan_product_path(related_products[3].id)
    end
  end
end
